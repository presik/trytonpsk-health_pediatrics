# -*- coding: utf-8 -*-

from trytond.pool import Pool
import pediatrics


def register():
    Pool.register(
        pediatrics.Newborn,
        pediatrics.NeonatalApgar,
        pediatrics.NeonatalMedication,
        pediatrics.NeonatalCongenitalDiseases,
        pediatrics.PediatricSymptomsChecklist,
        module='health_pediatrics', type_='model')
